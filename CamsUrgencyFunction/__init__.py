import logging

import azure.functions as func
import json
import os
import datetime
from . import urgency


def calculate_urgency_terms(name: str, task: dict):
    # Set Defaults
    MAX_AGE = int(os.environ.get("MAX_AGE", 180))
    AGE_WEIGHT = int(os.environ.get("AGE_WEIGHT", 0))
    NUM_LINKS_WEIGHT = int(os.environ.get("NUM_LINKS_WEIGHT", 0))
    TIME_SPENT_WEIGHT = int(os.environ.get("TIME_SPENT_WEIGHT", 0))
    PRIORITY_WEIGHT = int(os.environ.get("PRIORITY_WEIGHT", 0))
    BLOCKING_WEIGHT = int(os.environ.get("BLOCKING_WEIGHT", 0))
    BLOCKED_WEIGHT = int(os.environ.get("BLOCKED_WEIGHT", 0))
    PRIORITY_MAPPING_STR = os.environ.get(
        "PRIORITY_MAPPING", default='{"Low":-1, "Medium": 0, "High": 1}'
    )
    PRIORITY_MAPPING = json.loads(PRIORITY_MAPPING_STR)
    DATE_TIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f%z"

    # Age Term
    created_str = task["fields"]["created"]
    created_int = round(created_str / 1000)
    created = datetime.datetime.fromtimestamp(created_int, datetime.timezone.utc)
    age = datetime.datetime.now(datetime.timezone.utc) - created
    age_term = urgency.max_weight_term(age.days, AGE_WEIGHT, MAX_AGE)

    # Priority Term
    priority_str = task["fields"]["priority"]["name"]
    priority_term = urgency.category_term(
        priority_str, PRIORITY_WEIGHT, PRIORITY_MAPPING
    )

    # Number of Links
    num_links = len(task["fields"]["issuelinks"])
    num_links_term = urgency.fibonacci_term(num_links, NUM_LINKS_WEIGHT)

    # Time Spent on linked issues
    time_spent = task["fields"][
        "customfield_10350"
    ]  # Custom Field aggregated with automation
    time_spent_term = urgency.fibonacci_term(time_spent, TIME_SPENT_WEIGHT)

    # Blocking/Blocked terms
    blocking_term = 0
    if urgency.task_is_blocking(task):
        blocking_term = BLOCKING_WEIGHT
    blocked_term = 0
    if urgency.task_is_blocked(task):
        blocked_term = BLOCKED_WEIGHT
    # Total Term
    total = (
        age_term
        + priority_term
        + num_links_term
        + time_spent_term
        + blocked_term
        + blocking_term
    )

    return {
        "name": name,
        "total": round(total, 2),
        "age_term": age_term,
        "priority_term": priority_term,
        "num_links_term": num_links_term,
        "time_spent_term": time_spent_term,
        "is_blocked": urgency.task_is_blocked(task),
        "is_blocking": urgency.task_is_blocking(task),
    }


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info("Python HTTP trigger function processed a request.")
    try:
        req_body = req.get_json()
        name = req_body.get("key")
        item_urgency = calculate_urgency_terms(name, req_body)
    except (ValueError, TypeError, KeyError) as e:
        logging.error(f"There was a problem with the following request:")
        logging.error(f"Parameters: {dict(req.params)}")
        logging.error(f"Route Parameters: {dict(req.route_params)}")
        logging.error(f"Headers: {dict(req.headers)}")
        logging.error(f"Decoded Body: {req.get_body().decode()}")
        logging.error(f"{e}")
        return func.HttpResponse(
            "This function ran into an error and could not process the request",
            status_code=400,
        )

    return func.HttpResponse(json.dumps(item_urgency))
