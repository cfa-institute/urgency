# Create some functions to generate new cols with
from functools import lru_cache

# Helper: Get nth fibonacci number
@lru_cache
def _fib(n: int):
    if n <= 1:
        return n
    else:
        return _fib(n - 1) + _fib(n - 2)


@lru_cache
def max_weight_term(measure_item: int, weight: float, maximum_value: int) -> float:
    """A term that has a maximum value at which it will attain max weight. Great for age values"""
    if measure_item:
        if measure_item >= maximum_value:
            return 1 * weight
        return round((measure_item / maximum_value) * weight, 2)
    return 0


@lru_cache
def fibonacci_term(measure_item: float, weight: float) -> float:
    """A term that can increase infinitely, but the weight should not increase proportionally, like time spent or num linked incidents"""
    if measure_item:
        counter = 1
        grief = 0
        while grief <= measure_item:
            counter += 1
            grief = _fib(counter)
        return round(counter * 0.1 * weight, 2)
    return 0


def category_term(measure_item: str, weight: float, mapping) -> float:
    """A term that takes in a dict of mappings to weights. Great for things like high, med, low"""
    if measure_item and measure_item in mapping.keys():
        return round(float(mapping[measure_item]) * weight, 2)
    return 0


def task_is_blocked(task: dict) -> bool:
    """
    Check if the issue link is blocking issue in question, and if that issue link is not closed
    Status Category 3 here encompasses all issues in some final state, like 'Done', 'Cancelled', 'Closed', 'Complete'
    """
    issue_links = task["fields"].get("issuelinks", None)
    if issue_links:
        for issue in issue_links:
            inward_issue_type = issue["type"]["inward"]
            inward_issue_exists = issue.get("inwardIssue", None)
            if inward_issue_exists:
                inward_issue_status_category = issue["inwardIssue"]["fields"]["status"][
                    "statusCategory"
                ]["id"]
                if (
                    inward_issue_type == "is blocked by"
                    and inward_issue_status_category != 3
                ):
                    return True
    return False


def task_is_blocking(task: dict) -> bool:
    issue_links = task["fields"].get("issuelinks", None)
    if issue_links:
        for issue in issue_links:
            outword_issue_type = issue["type"]["outward"]
            outward_issue_exists = issue.get("outwardIssue", None)
            if outward_issue_exists:
                outward_issue_status_category = issue["outwardIssue"]["fields"][
                    "status"
                ]["statusCategory"]["id"]
                if (
                    outword_issue_type == "blocks"
                    and outward_issue_status_category != 3
                ):
                    return True
    return False
