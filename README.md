# Cam's Urgency Function

Coming originally from this [Problem Management Priority Proposal](https://cfainstitute.atlassian.net/wiki/spaces/JSDI/pages/896925910/Problem+Management+Priority+Proposal).

## How We Currently Sort

It depends on the person. Thats the issue. We desire a metric that accounts for multiple different features of specific incident types. 

For example, the default sorting on the [problem dashboard](https://cfainstitute.atlassian.net/secure/Dashboard.jspa?selectPageId=10035) for the hitlist is Total Number of Linked Incidents, and can only be sorted by one item.

## How We'd Like To Sort

We'd like to account for Total Number of Linked Incidents, time spent on those incidents, a manually set priority metric, age, staleness. 

## Development

1. Clone
2. `pip install -r requirements.txt`
3. `pip install -r requirements-dev.txt`
    - ALT to 2 and 3 (if you don't want to conflict with system packages)
        1. `pip install pipenv`
        2. `pipenv install --dev`
        3. `pipenv shell`
4. Make changes
5. `black .`
6. `coverage run --source=. -m pytest -vv`
7. `coverage report -m --skip-covered --fail-under=100`
8. `bandit -r CamsUrgencyFunction/` 
    - ALT to 5-8 `pre-commit run --all-files`
9. `git add .`
10. `cz commit` (follow prompt)