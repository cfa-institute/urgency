import pytest
import CamsUrgencyFunction.urgency


def test__fib_one():
    assert CamsUrgencyFunction.urgency._fib(1) == 1


def test__fib_high():
    # Thanks http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/fibtable.html
    assert (
        CamsUrgencyFunction.urgency._fib(200)
        == 280571172992510140037611932413038677189525
    )


def test_max_weight_term_at_max():
    """
    If the measured item is at or above the max, return the weight.
    """
    assert (
        CamsUrgencyFunction.urgency.max_weight_term(
            measure_item=10, weight=1.0, maximum_value=10
        )
        == 1
    )


def test_max_weight_term_over_max():
    assert (
        CamsUrgencyFunction.urgency.max_weight_term(
            measure_item=20, weight=1.0, maximum_value=10
        )
        == 1
    )


def test_max_term_less_max():
    """
    If the measure item is less than the max, multiply it's proportion of the max by the weight, and round to 2 points
    If the weight is 5, the max is 10, and the item being measured is 3:
        (3/10) * 5 = 1.5
    """
    assert (
        CamsUrgencyFunction.urgency.max_weight_term(
            measure_item=3, weight=5, maximum_value=10
        )
        == 1.5
    )


def test_max_term_none():
    """
    Sometimes the field being measured comes over and won't have a value. This should force it to go to none.
    """
    assert (
        CamsUrgencyFunction.urgency.max_weight_term(
            measure_item=None, weight=5, maximum_value=10
        )
        == 0
    )


def test_fibonacci_term_small():
    """
    The fibonacci term is a unique beast. Its the only term type that can increase past weight.
    In the beginning of the fib term increasing, it should increase weight rapidly, but with negative acceleration.
    The weight measure should be hit around 40 items, but should increase past max weight, though slower
    """
    assert CamsUrgencyFunction.urgency.fibonacci_term(measure_item=2, weight=10) == 4


def test_fibonacci_term_next_small():
    assert CamsUrgencyFunction.urgency.fibonacci_term(measure_item=3, weight=10) == 5


def test_fibonacci_term_large():
    assert CamsUrgencyFunction.urgency.fibonacci_term(measure_item=20, weight=10) == 8


def test_fibonacci_term_make_weight():
    assert CamsUrgencyFunction.urgency.fibonacci_term(measure_item=40, weight=10) == 10


def test_fibonacci_term_huge():
    assert CamsUrgencyFunction.urgency.fibonacci_term(measure_item=400, weight=10) == 15


def test_fibonacci_term_none():
    assert CamsUrgencyFunction.urgency.fibonacci_term(measure_item=None, weight=10) == 0


def test_category_term_low():
    """
    The mapping makes 'low' category take a negative weight.
    """
    assert (
        CamsUrgencyFunction.urgency.category_term(
            measure_item="Low", weight=10, mapping={"Low": -1, "Medium": 0, "High": 1}
        )
        == -10
    )


def test_category_term_medium():
    """
    The mapping makes 'medium' category take a no weight.
    """
    assert (
        CamsUrgencyFunction.urgency.category_term(
            measure_item="Medium",
            weight=10,
            mapping={"Low": -1, "Medium": 0, "High": 1},
        )
        == 0
    )


def test_category_term_high():
    """
    The mapping makes 'High' category take a full weight.
    """
    assert (
        CamsUrgencyFunction.urgency.category_term(
            measure_item="High", weight=10, mapping={"Low": -1, "Medium": 0, "High": 1}
        )
        == 10
    )


def test_category_term_no_mapping():
    """
    The category should not handle the bad value and zero out
    """
    assert (
        CamsUrgencyFunction.urgency.category_term(
            measure_item="Unhandled",
            weight=10,
            mapping={"Low": -1, "Medium": 0, "High": 1},
        )
        == 0
    )


def test_category_term_none():
    """
    The category should not handle the bad value and zero out
    """
    assert (
        CamsUrgencyFunction.urgency.category_term(
            measure_item=None, weight=10, mapping={"Low": -1, "Medium": 0, "High": 1}
        )
        == 0
    )


def test_task_is_blocked_true(blocked_task):
    """
    If the task has another task blocking it, return true
    Get blocked and blocking task from fixtures in conftest.py
    """
    assert CamsUrgencyFunction.urgency.task_is_blocked(blocked_task) == True


def test_task_is_blocked_false(blocking_task):
    """
    If the task does not have another task blocking it, return false
    """
    assert CamsUrgencyFunction.urgency.task_is_blocked(blocking_task) == False


def test_task_is_blocking_true(blocking_task):
    """
    If the task is blocking another task, return true
    """
    assert CamsUrgencyFunction.urgency.task_is_blocking(blocking_task) == True


def test_task_is_blocking_false(blocked_task):
    """
    If the task is not blocking another task, return false
    """
    assert CamsUrgencyFunction.urgency.task_is_blocking(blocked_task) == False
