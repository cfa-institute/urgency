import datetime
import pytest
import CamsUrgencyFunction
import azure.functions as func
import json


def test_calulate_urgency_terms_no_weights(blocking_task, patch_datetime_now):
    assert CamsUrgencyFunction.calculate_urgency_terms("CAM-1234", blocking_task) == {
        "name": "CAM-1234",
        "total": 0.0,
        "age_term": 0,
        "priority_term": 0.0,
        "num_links_term": 0.0,
        "time_spent_term": 0,
        "is_blocked": False,
        "is_blocking": True,
    }


def test_calulate_urgency_terms_even_weights_blocking(
    blocking_task, patch_datetime_now, monkeypatch
):
    """
    `blocking_task` has a low priority, and only has a single link, and is obviously blocking
    """
    monkeypatch.setenv("MAX_AGE", "180")
    monkeypatch.setenv("AGE_WEIGHT", "1")
    monkeypatch.setenv("NUM_LINKS_WEIGHT", "1")
    monkeypatch.setenv("TIME_SPENT_WEIGHT", "1")
    monkeypatch.setenv("PRIORITY_WEIGHT", "1")
    monkeypatch.setenv("BLOCKING_WEIGHT", "1")
    monkeypatch.setenv("BLOCKED_WEIGHT", "1")
    monkeypatch.setenv("PRIORITY_MAPPING", '{"Low":-1, "Medium": 0, "High": 1}')
    assert CamsUrgencyFunction.calculate_urgency_terms("CAM-1234", blocking_task) == {
        "name": "CAM-1234",
        "total": 0.3,
        "age_term": 0,
        "priority_term": -1.0,
        "num_links_term": 0.3,
        "time_spent_term": 0,
        "is_blocked": False,
        "is_blocking": True,
    }


def test_calulate_urgency_terms_even_weights(
    blocked_task, patch_datetime_now, monkeypatch
):
    """
    `blocked_task` has a low priority, and only has a single link, and is obviously blocking
    """
    monkeypatch.setenv("MAX_AGE", "180")
    monkeypatch.setenv("AGE_WEIGHT", "1")
    monkeypatch.setenv("NUM_LINKS_WEIGHT", "1")
    monkeypatch.setenv("TIME_SPENT_WEIGHT", "1")
    monkeypatch.setenv("PRIORITY_WEIGHT", "1")
    monkeypatch.setenv("BLOCKING_WEIGHT", "1")
    monkeypatch.setenv("BLOCKED_WEIGHT", "1")
    monkeypatch.setenv("PRIORITY_MAPPING", '{"Low":-1, "Medium": 0, "High": 1}')
    assert CamsUrgencyFunction.calculate_urgency_terms("CAM-1234", blocked_task) == {
        "name": "CAM-1234",
        "total": 0.3,
        "age_term": 0,
        "priority_term": -1.0,
        "num_links_term": 0.3,
        "time_spent_term": 0,
        "is_blocked": True,
        "is_blocking": False,
    }


def test_main_function_happy_path(raw_blocked_task):
    req = func.HttpRequest(
        method="GET", body=raw_blocked_task, url="/api/CamsUrgencyFunction"
    )

    resp = CamsUrgencyFunction.main(req)
    assert json.loads(resp.get_body()) == {
        "name": "JSDI-820",
        "total": 0.0,
        "age_term": 0,
        "priority_term": 0.0,
        "num_links_term": 0.0,
        "time_spent_term": 0,
        "is_blocked": True,
        "is_blocking": False,
    }


def test_main_function_less_happy_path(
    raw_blocking_task, monkeypatch, patch_datetime_now
):
    monkeypatch.setenv("MAX_AGE", "180")
    monkeypatch.setenv("AGE_WEIGHT", "1")
    monkeypatch.setenv("NUM_LINKS_WEIGHT", "1")
    monkeypatch.setenv("TIME_SPENT_WEIGHT", "1")
    monkeypatch.setenv("PRIORITY_WEIGHT", "1")
    monkeypatch.setenv("BLOCKING_WEIGHT", "1")
    monkeypatch.setenv("BLOCKED_WEIGHT", "1")
    monkeypatch.setenv("PRIORITY_MAPPING", '{"Low":-1, "Medium": 0, "High": 1}')
    req = func.HttpRequest(
        method="GET", body=raw_blocking_task, url="/api/CamsUrgencyFunction"
    )

    resp = CamsUrgencyFunction.main(req)
    assert json.loads(resp.get_body()) == {
        "name": "JSDI-819",
        "total": 0.3,
        "age_term": 0,
        "priority_term": -1.0,
        "num_links_term": 0.3,
        "time_spent_term": 0,
        "is_blocked": False,
        "is_blocking": True,
    }


def test_main_function_sad_path():
    req = func.HttpRequest(
        method="GET",
        body=b'{"this_object": "is busted"}',
        url="/api/CamsUrgencyFunction",
    )

    resp = CamsUrgencyFunction.main(req)
    assert (
        resp.get_body()
        == b"This function ran into an error and could not process the request"
    )
    assert resp.status_code == 400
