import pytest
import json
import datetime


@pytest.fixture()
def blocking_task():
    with open("tests/blocking_task.json") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture()
def blocked_task():
    with open("tests/blocked_task.json") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture()
def raw_blocking_task():
    with open("tests/blocking_task.json", "rb") as jsonfile:
        return jsonfile.read()


@pytest.fixture()
def raw_blocked_task():
    with open("tests/blocked_task.json", "rb") as jsonfile:
        return jsonfile.read()


FAKE_TIME = datetime.datetime(
    2021, 6, 26, 2, 13, 51, 814863, tzinfo=datetime.timezone.utc
)


@pytest.fixture
def patch_datetime_now(monkeypatch):
    class mydatetime(datetime.datetime):
        @classmethod
        def now(cls, tzinfo=datetime.timezone.utc):
            return FAKE_TIME

    monkeypatch.setattr(datetime, "datetime", mydatetime)
